'use strict';

const express = require('express');

module.exports = (projectService, userService) => {

    const project = express.Router();

    // При создании проекта указываем имя и описание. А уже авторов добавлять через другой запрос ( PUT: '/:id/users')
    project.post('/', (request, response) => {
        let params = request.body;
        projectService.createProject(params).then((result) => {
            response.json(result);
        }).catch((error) => {
            response.json(error);
        })
    });

    project.get('/', (request, response) => {
        let params = request.params;
        projectService.getProjects(params).then((result) => {
            response.json(result);
        }).catch((error) => {
            response.json(error);
        })
    });

    // Добавление новых участников проекта (users)
    project.put('/:id/users', (request, response) => {
        let params = request.body;
        let id = request.params.id;
        projectService.getProjectById(id).then((project) => {
            if (project === null) throw  ({ message: 'This project ID is not found' });
            return userService.getUserById(params.user_id).then((user) => {
                if (user === null) throw  ({ message: 'This user ID is not found' });
                return userService.addProject(params.user_id, project, user).then(() => {
                    return projectService.addUserInList(id, project, user).then((result) => {
                        response.json(result);
                    })
                });
            })
        }).catch((error) => {
            response.json(error);
        });
    });

    project.put('/:id', (request, response) => {
        let params = request.body;
        let id = request.params.id;
        projectService.updateProject(id, params).then((result) => {
            response.json(result);
        }).catch((error) => {
            response.json(error);
        })
    });

    project.delete('/:id', (request, response) => {
        let id = request.params.id;
        projectService.getProjectById(id).then((project) => {
            if (project === null) throw  ({ message: 'This project ID is not found' });
            let users = project.users;
            return projectService.deleteProject(id).then((result) => {
                return userService.deleteProject(users, id).then((mes) => {
                    response.json(mes);
                })
            })
        }).catch((error) => {
            response.json(error);
        })
    });

    return project;
};