'use strict';

const express = require('express');
const mail_sender = require('../utils/mail-sender')();
const cron = require('../utils/cron') ();
const jwt = require('jsonwebtoken');
const crypt = require('bcryptjs');
const config = require('../config.json');

const jobs_list = require('../utils/job-list');

module.exports = (userService, projectService) => {

    const user = express.Router();

    user.post('/', (request, response) => {
        let params = request.body;
        if (request.cookies.token === undefined) {
            params.password = crypt.hashSync(params.password, 8);
            userService.createUser(params).then((result) => {
                let token = jwt.sign({ user_id: result._id, role: result.role}, config.jwt.key, { expiresIn: config.jwt.time});
                response.cookie('token', token);
                response.json(result);
            }).catch((error) => {
                response.json(error);
            })
        }
        else response.json({ message: 'logout, please!' });
        /*let params = request.body;
        userService.createUser(params).then((result) => {
            response.json(result);
        }).catch((error) => {
            response.json(error);  // выводим смс об ошибке в случае если даже 1 поле не задано или равно 0, лиюо уже есть такая команда
        })*/
    });

    user.get('/', (request, response) => {
        let params = request.params;
        userService.getUsers(params).then((result) => {
            response.json(result);
        }).catch((error) => {
            response.json(error);
        })
    });

    // off/off
    // При первом запросе и каждом следующем нечетном 1,3,5... Именно для данного пользователя включается cron.start()
    // При каждом втором и следующих четных 2,4,6 - выключается stop()
    user.get('/:id/notice', (request, response) => {
        let params = request.params;
        if (params.id === response.user_id) {
            userService.getUserById(params.id).then((user) => {
                if (user === null) throw ({message: 'This user ID is not found'});
                let isIn = false;
                for (let i = 0; i < jobs_list.length; i++) {
                    if (jobs_list[i].user_id === user._id) {
                        jobs_list[i].job.stop();
                        response.json({message: 'Job for this user stopped'});
                        isIn = true;
                    }
                }
                if (!isIn) {
                    let job = cron.setSchedule(mail_sender.setEmailOptions, mail_sender.sendEmail, user);
                    job.start();
                    jobs_list.push({user_id: user._id, job: job});
                    response.json({message: 'Job is start'})
                }
            }).catch((error) => {
                response.json(error);
            })
        } else response.json({ message: 'Not permissions' })
    });

    user.put('/:id/projects', (request, response) => {
        let params = request.body;
        let id = request.params.id;
        userService.getUserById(id).then((user) => {
            if (user === null) throw ({ message: 'This user ID is not found' });
            return projectService.getProjectById(params.project_id).then((project) => {  // project_id (Postman) --body
                if (project === null) throw  ({ message: 'This project ID is not found' });
                return projectService.addUserInList(params.project_id, project, user).then(() => {
                    return userService.addProject(id, project, user).then((result) => {
                        response.json(result);
                    })
                });
            })
        }).catch((error) => {
            response.json(error);
        });
    });

    user.put('/:id', (request, response) => {
        let params = request.body;
        let id = request.params.id;
        if (response.role === 'admin' || id === response.user_id) {
            userService.getUserById(id).then((user) => {
                if (user === undefined) throw ({message: 'This user ID is not found'});
                params.role = user.role;
                params.password = user.password;
                return userService.updateUser(id, params).then((result) => {
                    response.json(result);
                })
            }).catch((error) => {
                response.json(error);
            })
        } else response.json({ message: 'Not permissions' })
    });

    user.delete('/:id', (request, response) => {
        if (response.role === 'admin') {
            let id = request.params.id;
            userService.getUserById(id).then((user) => {
                if (user === null) throw ({ message: 'This user ID is not found' });
                let projects = user.projects;
                return userService.deleteUser(id).then((user) => {
                    return projectService.deleteUser(projects, id).then((mes) => {
                        response.json(mes);
                    })
                })
            }).catch((error) => {
                response.json(error);
            })
        } else response.json({ message: 'Not permissions' })
    });

    return user;
};