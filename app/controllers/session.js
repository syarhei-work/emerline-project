'use strict';

const express = require('express');
const jwt = require('jsonwebtoken');
const crypt = require('bcryptjs');
const config = require('../config.json');

module.exports = (userService) => {
    const session = express.Router();

    // авторизация
    session.post('/', (request, response) => {
        let params = request.body;
        userService.getUserById(params.user_id).then((user) => {
            if (user === null) throw ({ message: 'This user ID is not found'});
            let test = crypt.compareSync(params.password, user.password);
            if (!test) {
                response.json({ message: 'This password is not define' });
            }
            else {
                let token = jwt.sign({ user_id: params.user_id, role: user.role}, config.jwt.key, { expiresIn: config.jwt.time});
                response.cookie('token', token);
                response.json({message: 'Hello , ' + user.first_name + ' ' + user.last_name + ' (' + params.user_id + ')'});
            }
        }).catch((error) => {
            response.json(error);
        })
    });

    session.delete('/', (request, response) => {
        response.clearCookie('token');
        response.json({message: 'logout'});
    });

    return session;
};