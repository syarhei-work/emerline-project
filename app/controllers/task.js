'use strict';

const express = require('express');

module.exports = (taskService, userService, projectService) => {

    const task = express.Router();

    task.post('/', (request, response) => {
        let params = request.body;
        //if (!moment(params.due_date, 'YYYY-MM-DD').isValid()) response.json({ message: 'Field due_date is invalid' });
        userService.getUserById(params.user_id).then((user) => {
            if (user === null) throw  ({ message: 'This user ID is not found' });
            return projectService.getProjectById(params.project_id).then((project) => {
                if (project === null) throw  ({ message: 'This project ID is not found' });
                params.user = user;
                params.project = project;
                taskService.createTask(params).then((result) => {
                    response.json(result);
                })
            })
        }).catch((error) => {
            response.json(error);
        })
    });

    task.get('/', (request, response) => {
        let params = request.params;
        taskService.getTasks(params).then((result) => {
            response.json(result);
        }).catch((error) => {
            response.json(error);
        })
    });

    task.put('/:id', (request, response) => {
        let params = request.body;
        let id = request.params.id;
        taskService.updateTask(id, params).then((result) => {
            response.json(result);
        }).catch((error) => {
            response.json(error);
        })
    });

    task.delete('/:id', (request, response) => {
        let id = request.params.id;
        taskService.deleteTask(id).then((result) => {
            response.json(result);
        }).catch((error) => {
            response.json(error);
        })
    });

    return task;
};