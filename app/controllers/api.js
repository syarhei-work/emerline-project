'use strict';

const express = require('express');

module.exports = (userService, taskService, projectService) => {
    const router = express.Router();

    let userController = require('./user') (userService, projectService);
    let taskController = require('./task') (taskService, userService, projectService);
    let projectController = require('./project') (projectService, userService);
    let sessionController = require('./session') (userService);

    router.use('/users', userController);
    router.use('/tasks', taskController);
    router.use('/projects', projectController);
    router.use('/sessions', sessionController);

    return router;
};