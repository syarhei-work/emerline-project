const cron = require('cron');

module.exports = () => {
    function Cron() {
        this.setSchedule = setSchedule;

        // * * 12 * * *
        // Каждые сутки в 12 часов дня
        function setSchedule(callback_init, callback_send, user) {
            return new cron.CronJob('*/5 * * * * *', () => {
                let server = callback_init();
                callback_send(server, user);
                console.log('SMS has been sent to user ' + user._id);
            }, () => {
                console.log('Job for user[id=' + user._id + '] stopped');
            }, false);
        }
    }

    return new Cron();
};