const email = require('emailjs/email');
const config = require('../config.json');

module.exports = () => {
    function Mailer() {
        this.setEmailOptions =setEmailOptions;
        this.sendEmail = sendEmail;
        this.emailValidate = emailValidate;

        function setEmailOptions() {
            return email.server.connect({
                user: config.email.sender,
                password: config.email.password,
                host: "smtp.gmail.com",
                ssl: true
            });
        }
        
        function sendEmail(server, user, response) {
            let first_name = user.first_name;
            let last_name = user.last_name;
            let user_email = user.email;
            server.send({
                text: "Good afternoon, " + first_name + " " + last_name + "!",
                from: "Server Bot <" + config.email.sender + ">",
                to: user_email,
                subject: "Emerline Notification"
            }, (err) => {   //  второй параметр - подробная инфа об отправленном смс
                if (!(response === undefined))
                    response.json(err || { message: 'SMS has been sent' });
            });
        }

        function emailValidate(user_email) {
            //  Хз как сделать проверку на наличие/существование такой почты...
        }
    }

    return new Mailer();
};