'use strict';

const jwt = require('jsonwebtoken');
const config = require('../config.json');
const permissions = require('./paths/permissions.json');

module.exports = () => {

    function checkPermission(client_type, method, path) {
        let paths = permissions[client_type];
        let isIn = false;
        for (let path_method in paths) {
            if (path_method === method) {
                if (paths.hasOwnProperty(path_method)) {
                    let urls = paths[path_method];
                    for (let url in urls) {
                        if (urls.hasOwnProperty(url))
                            if (urls[url] === path)
                                isIn = true;
                    }
                }
            }
        }
        return isIn;
    }

    return (request, response, next) => {
        let path = request._parsedUrl.pathname;
        let method = request.method;
        let token = request.cookies.token;
        if (token === undefined) {
            let isIn = checkPermission('guest', method, path);
            if (isIn === false) response.json({ error: 'Not permissions' });
            else next();
        }
        else {
            jwt.verify(token, config.jwt.key, (error, data) => {
                if (data === undefined) response.json({ error: 'Login, token is timing off'});
                else {
                    response.user_id = data.user_id;
                    response.role = data.role;
                    let role = data.role;
                    if (role === 'admin') next();
                    else if (role === 'manager' && (method === 'PUT' || method === 'GET' || method === 'DELETE')) next();
                    else if (method === 'GET') next();
                    else {
                        let isIn = checkPermission(role, method, path);
                        if (isIn === false) response.json({error: 'Not permissions'});
                        else next();
                    }
                }
            });
        }
    }
};