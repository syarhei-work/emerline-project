module.exports = (projectModel) => {
    function ProjectService(projectModel) {
        this.createProject = createProject;
        this.getProjects = getProjects;
        this.getProjectById = getProjectById;
        this.updateProject = updateProject;
        this.addUserInList = addUser;
        this.deleteUser = deleteUser;
        this.deleteProject = deleteProject;

        function createProject(options) {
            return new Promise((resolve, reject) => {
                projectModel.create(options, (error, project) => {
                    if (error) reject(error);
                    resolve(project)
                });
            })
        }

        function getProjects() {
            return new Promise((resolve, reject) => {
                projectModel.find({}).populate('users').exec((error, projects) => {
                    if (error) reject(error);
                    resolve(projects);
                })
            })
        }

        function getProjectById(id) {
            return new Promise((resolve, reject) => {
                projectModel.findById(id, (error, user) => {
                    if (error) reject(error);
                    resolve(user);
                })
            })
        }

        // Можно также использовать "ProjectModel.update(["условие"], ["то что менять"], [callback])... Но в данном случае удобнее и быстрее findByIdAndUpdate
        function updateProject(id, options) {
            return new Promise((resolve, reject) => {
                projectModel.findByIdAndUpdate(id, options, { new: true }, (error, project) => {
                    if (error) reject(error);
                    resolve(project);
                });
            })
        }

        function addUser(id, project, user) {
            return new Promise((resolve, reject) => {
                let users = project.users;
                let isIn = false;
                for (let i = 0; i < users.length; i++) {
                    if (users[i] === user._id) isIn = true;
                }
                if (!isIn) {
                    project.users.push(user._id);
                    projectModel.findByIdAndUpdate(id, {users: project.users}, {new: true}, (error, project) => {
                        if (error) reject(error);
                        resolve(project);
                    });
                } else reject({ message: 'This User ID is exists'});
            })
        }

        function deleteUser(ids, id) {
            return new Promise((resolve, reject) => {
                projectModel.find({ _id: { $in: ids }}).then((projects) => {
                    let promises = [];
                    for (let i = 0; i < projects.length; i++) {
                        let users = projects[i].users, arr = [];
                        for (let j = 0; j < users.length; j++) {
                            if (users[j] != id) arr.push(users[j]);
                        }
                        promises.push(projectModel.findByIdAndUpdate(projects[i]._id, { users: arr }))
                    }
                    return Promise.all(promises).then(resolve).catch(reject);
                }).catch(reject);

            })
        }

        // Также как и в предыдущей функции можно использовать основной метод "remove", но для упрощения будет вызывать
        function deleteProject(id) {
            return new Promise((resolve, reject) => {
                projectModel.findByIdAndRemove(id, (error, result) => {
                    if (error) reject(error);
                    resolve(result);
                })
            })
        }

    }

    return new ProjectService(projectModel);
};