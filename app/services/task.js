module.exports = (taskModel) => {
    function TaskService(taskModel) {
        this.createTask = createTask;
        this.getTasks = getTasks;
        this.updateTask = updateTask;
        this.deleteTask = deleteTask;

        function createTask(options) {
            return new Promise((resolve, reject) => {
                taskModel.create(options, (error, task) => {
                    if (error) reject(error);
                    resolve(task)
                });
            })
        }

        function getTasks() {
            return new Promise((resolve, reject) => {
                taskModel.find({}).populate('user').populate('project').exec((error, tasks) => {
                    if (error) reject(error);
                    resolve(tasks);
                })
            })
        }

        // Можно также использовать "TaskModel.update(["условие"], ["то что менять"], [callback])... Но в данном случае удобнее и быстрее findByIdAndUpdate
        function updateTask(id, options) {
            return new Promise((resolve, reject) => {
                taskModel.findByIdAndUpdate(id, options, { new: true }, (error, task) => {
                    if (error) reject(error);
                    resolve(task);
                });
            })
        }

        // Также как и в предыдущей функции можно использовать основной метод "remove", но для упрощения будет вызывать
        function deleteTask(id) {
            return new Promise((resolve, reject) => {
                taskModel.findByIdAndRemove(id, (error, result) => {
                    if (error) reject(error);
                    resolve(result);
                })
            })
        }

    }

    return new TaskService(taskModel);
};