module.exports = (userModel) => {
    function UserService(userModel) {
        this.createUser = createUser;
        this.getUsers = getUsers;
        this.getUserById = getUserById;
        this.updateUser = updateUser;
        this.addProject = addProject;
        this.deleteProject = deleteProject;
        this.deleteUser = deleteUser;

        function createUser(options) {
            return new Promise((resolve, reject) => {
                userModel.create(options, (error, user) => {
                    if (error) reject(error);
                    resolve(user)
                });
            })
        }

        function getUsers() {
            return new Promise((resolve, reject) => {
                userModel.find({}).populate('projects').exec((error, projects) => {
                    if (error) reject(error);
                    resolve(projects);
                })
            })
        }

        function getUserById(id) {
            return new Promise((resolve, reject) => {
                userModel.findById(id, (error, user) => {
                    if (error) reject(error);
                    resolve(user);
                })
            })
        }

        // Можно также использовать "userModel.update(["условие"], ["то что менять"], [callback])... Но в данном случае удобнее и быстрее findByIdAndUpdate
        function updateUser(id, options) {
            return new Promise((resolve, reject) => {
                userModel.findByIdAndUpdate(id, options, { new: true }, (error, user) => {
                    if (error) reject(error);
                    resolve(user);
                });
                /*userModel.updateUser({ _id: id }, options, (error, user) => {
                    if (error) reject(error);
                    resolve(user);
                });*/
                /*userModel.findOneAndUpdate(id, options, (error, user) => {
                    if (error) reject(error);
                    resolve(user);
                })*/
            })
        }

        function addProject(id, project, user) {
            return new Promise((resolve, reject) => {
                let projects = user.projects;
                let isIn = false;
                for (let i = 0; i < projects.length; i++) {
                    if (projects[i] === project._id) isIn = true;
                }
                if (!isIn) {
                    user.projects.push(project._id);
                    userModel.findByIdAndUpdate(id, {projects: user.projects}, {new: true}, (error, project) => {
                        if (error) reject(error);
                        resolve(project);
                    });
                } else reject({ message: 'This User ID is exists'});
            })
        }

        function deleteProject(ids, id) {
            return new Promise((resolve, reject) => {
                userModel.find({ _id: { $in: ids }}).then((users) => {
                    let promises = [];
                    for (let i = 0; i < users.length; i++) {
                        let projects = users[i].projects, arr = [];
                        for (let j = 0; j < projects.length; j++) {
                            if (projects[j] != id) arr.push(projects[j]);
                        }
                        promises.push(userModel.findByIdAndUpdate(users[i]._id, { projects: arr }))
                    }
                    return Promise.all(promises).then(resolve).catch(reject);
                }).catch(reject);

            })
        }

        // Также как и в предыдущей функции можно использовать основной метод "remove", но для упрощения будет вызывать
        function deleteUser(id) {
            return new Promise((resolve, reject) => {
                userModel.findByIdAndRemove(id, (error, result) => {
                    if (error) reject(error);
                    resolve(result);
                })
            })
        }

    }

    return new UserService(userModel);
};