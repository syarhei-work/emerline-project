'use strict';

module.exports = (mongoose, db, auto_increment) => {
    return db.model('task', new mongoose.Schema({
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: false
        },
        due_date: {
            type: Date
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user'
        },
        project: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'project'
        }
    }, {
        versionKey: false
    }).plugin(auto_increment.plugin, {
        model: 'task',
        field: '_id'  // при изменении поля нужно из бд удалять предыдущее созданное для избежания конфликта index полей. В нашем случае мы сделали авто-инкремент из сновного стандартного поля _id во избежания повторения однотипных полей (_id & user_id). ??? Возможно это происходит так как в БД ставится 2 индекса и 1 из этих индексов при создании новой строки в таблице будет всегда присваивать null. И получается что на 2 раз даже создавая разные объекты индексы будут совпадать. Для этого и следует каждый раз удалять педыдущий индекс.
    }))
};