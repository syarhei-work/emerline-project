'use strict';

module.exports = (mongoose, db, auto_increment) => {
    return db.model('user', new mongoose.Schema({
        first_name: {
            type: String,
            required: true
        },
        last_name: {
            type: String,
            required: true
        },
        role: {
            type: String,
            required: true,
            enum: [
                'manager',
                'developer',
                'admin'
            ]
        },
        email: {
            type: String,
            required: true,
            match: /\S+@\S+.\S+/  // Regex for email
        },
        password: {
            type: String,
            required: true  // Будем хэшировать пароль в строку (bcrypt.js)
        },
        projects: [{
            type: Number,
            ref: 'project'
        }]
    }, {
        versionKey: false
    }).plugin(auto_increment.plugin, {
        model: 'user',
        field: '_id'  // при изменении поля нужно из бд удалять предыдущее созданное для избежания конфликта index полей. В нашем случае мы сделали авто-инкремент из сновного стандартного поля _id во избежания повторения однотипных полей (_id & user_id). ??? Возможно это происходит так как в БД ставится 2 индекса и 1 из этих индексов при создании новой строки в таблице будет всегда присваивать null. И получается что на 2 раз даже создавая разные объекты индексы будут совпадать. Для этого и следует каждый раз удалять педыдущий индекс.
    }))
};