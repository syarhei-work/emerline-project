const express = require('express');
const mongoose = require('mongoose');
const body_parser = require('body-parser');
const cookie_parser = require('cookie-parser');


let app = express();

let mongodb_connection = require('./database/context') (mongoose);

let userService = require('./services/user') (mongodb_connection.user);
let taskService = require('./services/task') (mongodb_connection.task);
let projectService = require('./services/project') (mongodb_connection.project);

let api = require('./controllers/api') (userService, taskService, projectService);

let auth = require('./utils/token') ();

app.use(body_parser.json());
app.use(body_parser.urlencoded({
    extended: true
}));
app.use(cookie_parser());

app.use('/api', auth);
app.use('/api', api);

app.listen(3300, () => {
    console.log('Server is starting...');
});


/*
let UserSchema = new mongoose.Schema( {
    name: { type: String, default: "hahaha" },
    age: { type: Number, min: 18, index: true },
    bio: { type: String, match: /[a-z]/ },
    date: { type: Date },
    buff: Buffer
} );

UserSchema.methods.speak = function () {
    let greeting = this.name
        ? "My name is " + this.name
        : "I don't have a name";
    console.log(greeting);
};

let User = db.model("User",UserSchema);

let newUser = new User({ name: "Alice", age: 21});

newUser.save( (err, newUser) => {
    if (err){
        console.log("Something goes wrong with user " + newUser.name);
    }else{
        newUser.speak();
    }
});
*/

/*
let mongoClient = mongoose.MongoClient;

let url = "mongodb://localhost:27017/test";
mongoClient.connect(url, function(err, db){

    let collection = db.collection("users");
    let user = {name: "Tom", age: 23};
    collection.insertOne(user, function(err, result){

        if(err){
            return console.log(err);
        }
        console.log(result.ops);
        db.close();
    });
});*/