'use strict';

const auto_increment = require('mongoose-auto-increment');
const config = require('../config.json');

module.exports = (mongoose) => {

    let mongodb = config.mongodb;
    let db = mongoose.createConnection(
        'mongodb://' +
        mongodb.user + ':' +
        mongodb.password + '@' +
        mongodb.hostname + ':' +
        mongodb.port + '/' +
        mongodb.database
    );

    db.on("error", (error) => { console.log(error) });
    db.on("open", () => { console.log('Database connection is open!') });

    auto_increment.initialize(db);

    let user = require('../models/user') (mongoose, db, auto_increment);
    let task = require('../models/task') (mongoose, db, auto_increment);
    let project = require('../models/project') (mongoose, db, auto_increment);

    return {
        user: user,
        task: task,
        project: project
    };
};