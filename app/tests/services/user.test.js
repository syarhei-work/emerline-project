let users = [
    {
        _id : 0,
        first_name : 'sergei',
        last_name : 'murkou',
        role : 'manager',
        email : 'murkous@mail.ru',
        password : '123',
        projects : []
    },
    {
        _id : 2,
        first_name : 'iva',
        last_name : 'iva',
        role : 'developer',
        email : 'murkous@mail.ru',
        password : '123',
        projects : []
    }
];

let mockUser = require('../mock/base') (users);
let serviceUser = require('../../services/user') (mockUser);

describe('users', () => {
    it ('return promise', () => {
        expect(serviceUser.getUsers()).toBeInstanceOf(Promise);
    });
    it ('create user', async () => {
        let array = await serviceUser.createUser(users[0]);
        expect(array).toEqual(users[0]);
        expect(array).toBeInstanceOf(Object);
    });
    it('find user by ID', async () => {
        let user = await serviceUser.getUserById(2);
        expect(mockUser.findById).toHaveBeenCalled();
        expect(user).toEqual(users[1]);
        expect(user).toBeInstanceOf(Object);
    });
    it('update user', async () => {
        let options = {
            first: 'S',
            last_name: 'M'
        };
        let user = await serviceUser.updateUser(0, options);
        expect(mockUser.findByIdAndUpdate).toHaveBeenCalled();
        expect(user).toBeInstanceOf(Object);
    });
    it('delete user', async () => {
        let user = await serviceUser.deleteUser(0);
        expect(mockUser.findByIdAndRemove).toHaveBeenCalled();
        expect(user).toEqual(users[0]);
    });
});