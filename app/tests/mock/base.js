'use strict';

module.exports = (data) => {
    const mock = {
        create: jest.fn((data, callback) => callback(null, data)),
        find: jest.fn((params) => Promise.resolve(data)),
        findById: jest.fn((id, callback) => {
            data.forEach((item, i) => {
                let array = Object.keys(item);
                if (id === item[array[0]]) {
                    callback(null, item);
                }
            });
            callback(null, null);
        }),
        findByIdAndUpdate: jest.fn((id, object, params, callback) => {
            data.forEach((item, i) => {
                let array = Object.keys(item);
                if (id === item[array[0]]) {
                    callback(null, item);
                }
            });
            callback(null, null);
        }),
        findByIdAndRemove: jest.fn((id, callback) => {
            data.forEach((item, i) => {
                let array = Object.keys(item);
                if (id === item[array[0]]) {
                    callback(null, item);
                }
            });
            callback(null, null);
        })
    };

    mock.mockClear = () => {
        mock.create.mockClear();
        mock.find.mockClear();
        mock.findById.mockClear();
        mock.findByIdAndUpdate.mockClear();
        mock.findByIdAndRemove.mockClear();
    };

    return mock;
};