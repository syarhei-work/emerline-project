# Emerline First (Test) Project by Sergei Murkou

## Simple Node JS App using:

- MongoDB and ODM(ORM) mongoose
- express
- REST API
- JWT authentication
- bcrypt (passwords)
- emailjs (sending email)
- cron (creating jobs)
- unit tests (jest)